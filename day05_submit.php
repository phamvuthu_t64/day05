<!DOCTYPE html>
<html lang="en">
<?php session_start(); ?>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
    * {
        text-align: center;
    }

    fieldset {
        width: 470px;
        margin: 12%;
        margin-left: 32%;
        border: 1px solid #1E90FF;
    }

    .username {
        background-color: #5b9bd5;
        color: azure;
        padding: 10px 20px;
        margin-right: 25px;
        width: 100px;
        text-align: center;
        border: 1px solid #1E90FF
    }

    .text {
        border: 1px solid #1E90FF;
        padding: 10px;
        width: 278px;
    }

    .apartment {
        margin-top: 20px;

    }

    .department {
        width: 300px;
        border: 1px solid #1E90FF;
    }

    .button {
        width: 100px;
        margin-top: 30px;
        height: 30px;
        background-color: #008000;
        border: 1px solid #1E90FF;
        border-radius: 5px;
        color: white;
    }
    
    .button:hover {
    opacity: 0.8;
}

    .style {
        display: flex;
    }

    .text {
        border: 1px solid #1E90FF;
    }

    .notnull {
        color: red;
    }
</style>
<body>
    <form method="post">
        <fieldset class="day05-forn">
            <div class="forn">
                <div class="apartment">
                    <div class="style">
                        <div class="username">
                            <label>
                                Họ và tên <sup class="notnull">*</sup>
                            </label>
                        </div>
                        <td><p>
                        <?php echo $_SESSION["user_name"]; ?>
                        </p></td>
                    </div>
                </div>
                <div class="apartment">
                    <div class="style">
                        <div class="username">
                            <label>
                                Giới tính<sup class="notnull">*</sup>
                            </label>
                        </div>
                        <td><p>
                        <?php echo $_SESSION["gender"]; ?>
                        </p></td>
                    </div>
                </div>
                <div>
                </div>
            </div>
            </div>
            <div class="apartment">
                <div class="style">
                    <div class="username">
                        <label>
                            Phân khoa <sup class="notnull">*</sup>
                        </label>
                    </div>
                    <td><p>
                        <?php echo $_SESSION["department"]; ?>
                        </p></td>
                </div>
                <div class="apartment">
                    <div class="style">
                        <div class="username">
                            <label>
                                Sinh nhật <sup class="notnull">*</sup>
                            </label>
                        </div>
                        <td><p>
                        <?php echo $_SESSION["date"]; ?>
                        </p></td>
                    </div>
                </div>
                <div>
                    <div class="apartment">
                        <div class="style">
                            <div class="username">
                                <label>
                                    Địa chỉ
                                </label>
                            </div>
                            <td><p>
                        <?php echo $_SESSION["diachi"]; ?>
                        </p></td>
                        </div>
                    </div>
                </div>
                </div>
                <div>
                    <div class="apartment">
                        <div class="style">
                            <div class="username">
                                <label>
                                    Hình ảnh
                                </label>
                            </div>
                            <td>
                
                   <?php echo "<br> <img src = '" . $_SESSION["image"] ."  ' width = '100' height = '50'>";
                   echo "<br>"
                   ?>
                   </td>
                    
                        </div>
                    </div>
                </div>
                <input type="submit" name = "button" class="button" value="Đăng ký" />
            </div>

    </form>
    </fieldset>
</body>

</html>